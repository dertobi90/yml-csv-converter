<?php

if (!array_key_exists(1, $argv)) {
    $status = "filename missing\n";
} else {
    $status = csvToYaml($argv[1]);
}
echo $status;


function csvToYaml($filename)
{
    if (!is_string($filename) || $filename == '') {
        return "filename missing\n";
    }

    echo("process '" . $filename . "' : ");
    $file = file($filename);
    if (!$file) {
        return "file invalid\n";
    }

    $resultArray = array();
    $handle = fopen($filename, "r");
    while (($data = fgetcsv($handle)) !== FALSE) {
        $keys = array_reverse(explode('.', $data[0]));
        $subArray = array();
        $value = $data[1];
        foreach ($keys as $key) {
            $subArray = array($key => $value);
            $value = $subArray;
        }
        $resultArray = array_merge_recursive($resultArray, $subArray['root']);
    }

    if (!is_array($resultArray) || empty($resultArray)) {
        return "invalid or empty csv file\n";
    }

    // write to yaml
    $yamlFilename = str_replace('.csv', '.yml', $filename);
    $status = yaml_emit_file($yamlFilename, $resultArray, YAML_UTF8_ENCODING,   YAML_CR_BREAK);
    if ($status !== true) {
        return "error while writing in file\n";
    }

    return "success\n";
}