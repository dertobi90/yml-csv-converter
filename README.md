# yml-csv-converter

## Requirements

You need to install the PHP YAML Extension.
Therefore, please have a look into manuals like:
https://www.php.net/manual/de/yaml.installation.php
https://serverpilot.io/docs/how-to-install-the-php-yaml-extension

* In short you need to install at least libyaml-dev with apt and yaml with pecl:
    ```
    $ sudo apt-get install libyaml-dev
    $ sudo pecl install yaml
    ```
* Check installation:
    ```
    $ pecl info yaml
    ```

* Edit the ini file and add yaml extension:
    ```
    $ sudo nano /etc/php/7.0/cli/php.ini
    ```

* Add following line:
    ```
    extension=yaml.so
    ```

## convert yml/yaml to csv
* single file - convert a single .yml/.yaml file with given %filename% into a .csv file
    ```
    $ php ymltocsv.php %filename%
    ```
* bulk processing - convert all .yml files in this directory into .csv files
    ```
    $ sh processYaml.sh
    ```

## convert csv to yml
* single file - convert a single .csv file with given %filename% into a .yml file
    ```
    $ php csvtoyaml.php %filename%
    ```
* bulk processing - convert all .csv files in this directory into .yml files
    ```
    $ sh processCsv.sh
    ```