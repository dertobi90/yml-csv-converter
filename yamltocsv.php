<?php

if (!array_key_exists(1, $argv)) {
    $status = "filename missing\n";
} else {
    $status = yamlToCsv($argv[1]);
}
echo $status;

function recursiveArrayParse($parentKey, $element, $parentArray)
    {
        if (is_array($element)) {
            foreach ($element as $key => $value) {
                $parentArray = recursiveArrayParse($parentKey . '.' . $key, $value, $parentArray);
            }
        } else {
            $parentArray[$parentKey] = $element;
        }

        return $parentArray;
}

function yamlToCsv($filename)
{
	if (!is_string($filename) || $filename == '') {
	  return "filename missing\n";
	}

	echo("process '" . $filename . "' : ");
	// @fixme: yaml_parse_file seems to delete key "no" (e.g. key of country Norway)
	$yaml = yaml_parse_file($filename);
	if (!$yaml) {
	    return "yaml invalid\n";
    }
    $resultArray = recursiveArrayParse('root', $yaml, array());

    // write to csv
    $csvFilename = str_replace('yml', 'csv', str_replace('.yaml', '.csv', $filename));
    $fp = fopen($csvFilename, 'w');
	if ($fp === false) {
		return "unable to open file\n";
	}

    foreach ($resultArray as $key => $value) {
        $status = fputcsv($fp, array($key, $value));
        if ($status === false) {
            return "unable to write in file\n";
        }
    }

    $closeStatus = fclose($fp);
	if ($closeStatus === false) {
		return "error while writing in file\n";
	}

	return "success\n";
}
